#### Objective
The document enlists the current roles, responsibilties and corresponding representatives from NRAMC. The information provided herein can be used to contact individual members for relevant issues/suggestions etc.

#### The roles, responsibilties and representatives

|Role|Responsibilty|Representative|
|-----|-----|-----|
|President|||
|Vice-President|||
|Secretary|||
|Treasure|||
|Cultural Secretary|||
|Executive members|||
