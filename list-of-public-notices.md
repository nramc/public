#### Objective
This page maintains a list of all our notices broadcasted among Nilgiri residents

#### Notices
|Topic|Link|
|-----|-----|
|Dues collection Jan-June 2021|**[Link](https://gitlab.com/nramc/public/-/blob/master/notices/06_Due_Collection_1_.docx)**|
|Preventive measures regarding Covid-19||
