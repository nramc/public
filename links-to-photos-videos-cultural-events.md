#### Objective
The document is supposed to maintain WebURLS of Photos/Videos (and/or) other media created during our cultural events.

#### Links to cultural-event media

|Event|Link|
|-----|-----|
|Republic Day 2021| https://www.youtube.com/watch?v=0EvhXf4RZI4|
|Holi 2021|TBD|
