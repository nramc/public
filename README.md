# public

The `public` repository is supposed to host all of our public-facing information. Residents can suggest changes or comment on the existing content by creating Gitlab issues. Following is the list of content in this repository:

|Information|Weblink|
|-----|-----|
|NRAMC scope-of-activities|[**Link**](https://gitlab.com/nramc/public/-/blob/master/nramc-scope-of-activities.md)|
|NRAMC Roles, Responsibilties and Representatives|[**Link**](https://gitlab.com/nramc/public/-/blob/master/NRAMC-roles-and-responsibilties.md)|
|Public notices|[**Link**](https://gitlab.com/nramc/public/-/blob/master/notices.md)|
|Links to cultural-event photos/videos|[**Link**](https://gitlab.com/nramc/public/-/blob/master/links-to-photos-videos-cultural-events.md)|
|List of important numbers|[**Link**](https://gitlab.com/nramc/public/-/blob/master/list-of-important-numbers.md)|
|NRAMC members's Minutes of Meeting|[**Link**](https://gitlab.com/nramc/public/-/blob/master/NRAMC-minutes-of-meeting.md)|
